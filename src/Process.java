class Process {
    private int pid;
    private int cpu_priority;
    private int disk_used;
    private String file_name;

    Process() {
        pid = 1;
        cpu_priority = 1;
        disk_used = -1;
        file_name = "";
    }

    Process(int pid, int cpu_priority) {
        this.pid = pid;
        this.cpu_priority = cpu_priority;
        disk_used = -1;
        file_name = "";
    }

    void setDisk_used(int disk_used) {
        this.disk_used = disk_used;
    }

    void setFile_name(String file_name) {
        this.file_name = file_name;
    }

    int getCpu_priority() {
        return cpu_priority;
    }

    String getFile_name() {
        return file_name;
    }

    int getPid() {
        return pid;
    }
}
