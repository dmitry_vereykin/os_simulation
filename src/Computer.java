import java.util.*;

class Computer {

    private boolean running;
    private int pid_counter;
    private int time_stamp;
    private int frame_table_size;

    private int ram_size;
    private int page_size;
    private int number_of_disks;

    private LinkedList<Process> ready_queue;
    private ArrayList<LinkedList<Process>> disks;
    private int[][] frame_table;

    Computer(){
        running = true;
        ram_size = 100;
        page_size = 10;
        number_of_disks = 2;
        pid_counter = 1;
        time_stamp = 0;
        frame_table_size = ram_size / page_size;

        ready_queue = new LinkedList<>();
        disks = new ArrayList<>();
        frame_table = new int[frame_table_size][4];
        createDisks();
        createFrameTable();
    }

    Computer(int ram_size, int page_size, int number_of_disks) {
        running = true;
        this.ram_size = ram_size;
        this.page_size = page_size;
        this.number_of_disks = number_of_disks;
        pid_counter = 1;
        time_stamp = 0;
        frame_table_size = ram_size / page_size;

        ready_queue = new LinkedList<>();
        disks = new ArrayList<>();
        frame_table = new int[frame_table_size][4];
        createDisks();
        createFrameTable();
    }

    private void createDisks() {
        for (int i = 0; i < number_of_disks; i++) {
            disks.add(new LinkedList<>());
        }
    }

    private void createFrameTable() {
        for (int i = 0; i < frame_table_size; i++) {
            frame_table[i][0] = i;
        }
    }

    boolean addProcess(int priority) {
        Process p = new Process(pid_counter, priority);

        time_stamp++;
        addingInFrameTable(0, pid_counter);
        pid_counter++;

        if (ready_queue.isEmpty()) {
            ready_queue.addFirst(p);
            return true;
        } else if (p.getCpu_priority() > ready_queue.getFirst().getCpu_priority()) {
            iterate_and_add(ready_queue.removeFirst());
            ready_queue.addFirst(p);
        } else {
            iterate_and_add(p);
        }

        return false;
    }

    private boolean iterate_and_add(Process p) {
        int i = 0;
        while (ready_queue.size() > i) {
            if (p.getCpu_priority() > ready_queue.get(i).getCpu_priority()) {
                ready_queue.add(i, p);
                return true;
            }
            i++;
        }

        if (ready_queue.isEmpty()) {
            ready_queue.addFirst(p);
        } else {
            ready_queue.addLast(p);
        }

        return false;
    }

    boolean terminateProcess() {
        if (ready_queue.isEmpty())
            return false;
        else
            System.out.println("Process " + ready_queue.getFirst().getPid() + " terminated.");
        for (int i = 0; i < frame_table_size; i++) {
            if (frame_table[i][2] == ready_queue.getFirst().getPid()) {
                frame_table[i][1] = 0;
                frame_table[i][2] = 0;
                frame_table[i][3] = 0;
            }
        }
        ready_queue.removeFirst();
        return true;
    }

    boolean loadPage(int address) {
        time_stamp++;
        if (address < 0 || address > ram_size)
            return false;
        else {
            int page = address / page_size;
            addingInFrameTable(page, ready_queue.getFirst().getPid());
            return true;
        }
    }

    private boolean addingInFrameTable(int page, int pid) {

        for (int i = 0; i < frame_table_size; i++) {
            if (frame_table[i][1] == page && frame_table[i][2] == pid) {
                frame_table[i][3] = time_stamp;
                return false;
            }
        }

        int min = time_stamp;

        for (int i = 0; i < frame_table_size; i++) {
            if(frame_table[i][3] < min)
                min = frame_table[i][3];
        }

        for (int i = 0; i < frame_table_size; i++) {
            if(frame_table[i][3] == min) {
                frame_table[i][1] = page;
                frame_table[i][2] = pid;
                frame_table[i][3] = time_stamp;
                break;
            }
        }
        return true;
    }

    boolean requestFile(String str) {
        int space = str.indexOf(' ');
        int disk_number = -1;

        try {
            if (space >= 1)
                disk_number = Integer.parseInt(str.substring(0, space));
            else {
                System.out.println("Wrong format input!");
                return true;
            }
        } catch (NumberFormatException e) {
            //System.out.println(str.substring(0, space) + " is not a number.");
            return false;
        }

        String file_name = str.substring(space + 1);

        if (disk_number < 0 || disk_number >= number_of_disks || ready_queue.isEmpty())
            return false;
        else {
            ready_queue.getFirst().setDisk_used(disk_number);
            ready_queue.getFirst().setFile_name(file_name);
            disks.get(disk_number).add(ready_queue.getFirst());
            ready_queue.removeFirst();
            return true;
        }
    }

    boolean diskDoneWithProcess(int disk_number) {
        if (disk_number < 0 || disk_number >= number_of_disks || disks.get(disk_number).isEmpty()) {
            return false;
        } else {
            iterate_and_add(disks.get(disk_number).getFirst());
            disks.get(disk_number).removeFirst();
            return true;
        }
    }

    boolean printReadyQueue() {
        if (ready_queue.isEmpty()) {
            return false;
        } else {
            System.out.println("\n=============================");
            System.out.println("Process " + ready_queue.get(0).getPid() +
                    " (" + ready_queue.get(0).getCpu_priority() + ") is using CPU.");
            System.out.println("-----------------------------");
            System.out.printf("%-10s %s\n", "PID:", "Priority:");

            for (int i = 1; i < ready_queue.size(); i++) {
                System.out.printf("%-10d %d\n", ready_queue.get(i).getPid(), ready_queue.get(i).getCpu_priority());
            }
            System.out.println("=============================\n");
            return true;
        }
    }

    boolean printFrameTable() {
        if (ready_queue.isEmpty() && diskQueuesAreEmpty())
            return false;
        else {
            System.out.println("\n======================================");
            System.out.printf("%-10s %-10s %-10s %-10s\n", "Frame:", "Page:", "PID:", "Time:");
            for (int i = 0; i < frame_table_size; i++) {
                System.out.printf("%-10d %-10d %-10d %-10d\n",
                        frame_table[i][0], frame_table[i][1], frame_table[i][2], frame_table[i][3]);
            }
            System.out.println("======================================\n");
            return true;
        }
    }

    void printDisksState() {
        System.out.println("\n======================================");
        for (int i = 0; i < number_of_disks; i++) {
            if (disks.get(i).isEmpty()) {
                System.out.println("--------------------------------------");
                System.out.printf("%-10s %-10d %-10s\n", "Disk:", i, "EMPTY");
                System.out.println("--------------------------------------\n");
            } else {
                System.out.println("Process " + disks.get(i).getFirst().getPid() + " (\""
                        + disks.get(i).getFirst().getFile_name() + "\")" + " is using Disk " + i);
                System.out.println("--------------------------------------");
                System.out.printf("%-10s %d\n", "Disk:", i);
                System.out.printf("%-10s %s\n", "PID:", "File name:");
                for (int j = 1; j < disks.get(i).size(); j++) {
                    System.out.printf("%-10d %-10s\n", disks.get(i).get(j).getPid(),
                            disks.get(i).get(j).getFile_name());
                }
                System.out.println("");
            }
        }
        System.out.println("======================================\n");
    }

    private boolean diskQueuesAreEmpty() {
        for (int i = 0; i < number_of_disks; i++) {
            if (!disks.get(i).isEmpty())
                return false;
        }
        return true;
    }

    boolean isRunning() {
        return running;
    }

    void terminateComputer() {
        this.running = false;
    }
}
